// ---------- scribblearea.cpp ----------
#include <QTextStream>
#include <QtWidgets>
#include <QtDebug>
#include <QInputDialog>
#include <bits/stdc++.h>
#include <QInputDialog>
#include <QFontDialog>
#if defined(QT_PRINTSUPPORT_LIB)
#include <QtPrintSupport/qtprintsupportglobal.h>
#if QT_CONFIG(printdialog)
#include <QPrinter>
#include <QPrintDialog>
#include <QTextEdit>

#endif
#endif

#include "scribblearea.h"

ScribbleArea::ScribbleArea(QWidget *parent)
    : QWidget(parent)
{
    // Roots the widget to the top left even if resized
    setAttribute(Qt::WA_StaticContents);

    // Set defaults for the monitored variables
    modified = false;
    scribbling = false;
    myPenWidth = 1;
    myPenColor = Qt::blue;
    //myOldColor = myPenColor;
    action = 0;//1=color pick, 2=fill in
}

// Used to load the image and place it in the widget
bool ScribbleArea::openImage(const QString &fileName)
{
    // Holds the image
    QImage loadedImage;

    // If the image wasn't loaded leave this function
    if (!loadedImage.load(fileName))
        return false;

    QSize newSize = loadedImage.size().expandedTo(size());
    resizeImage(&loadedImage, newSize);
    image = loadedImage;
    modified = false;
    update();
    return true;
}

// Save the current image
bool ScribbleArea::saveImage(const QString &fileName, const char *fileFormat)
{
    // Created to hold the image
    QImage visibleImage = image;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    } else {
        return false;
    }
}

// Used to change the pen color
void ScribbleArea::setPenColor(const QColor &newColor)
{
    myPenColor = newColor;
}

// Used to change the pen width
void ScribbleArea::setPenWidth(int newWidth)
{
    myPenWidth = newWidth;
}
void ScribbleArea::setFillvariable()
{
    //if (action==3) {
    //    myPenColor=myOldColor;
    //}
    action=1;
}
void ScribbleArea::setPencilvariable()
{
    //if (action==3) {
    //    myPenColor=myOldColor;
    //}
    action=0;
}
void ScribbleArea::setColorPickvariable()
{
    //if (action==3) {
    //    myPenColor=myOldColor;
    //}
    action=2;
}
void ScribbleArea::setEraservariable()
{
    action=3;
}
void ScribbleArea::setTextvariable()
{
    //if (action==3) {
    //    myPenColor=myOldColor;
    //}
    action=4;
}

void ScribbleArea::setLinevariable()
{
    action=5;
}
void ScribbleArea::setRayvariable()
{
    action=6;
}
void ScribbleArea::setRectanglevariable()
{
    action=7;
}
void ScribbleArea::setCirclevariable()
{
    action=8;
}
void ScribbleArea::setTextFont(QFont NewText){
    TextFont=NewText;
}

// Color the image area with white
void ScribbleArea::clearImage()
{
    this->pushUndo();
    image.fill(qRgb(255, 255, 255));
    modified = true;
    update();
}
QRgb ScribbleArea::getColor(QPoint lastPoint){
    QPixmap pixmap(this->size());
        this->render(&pixmap);
        QImage img(pixmap.toImage());
        QRgb pix = img.pixel(lastPoint.x(),lastPoint.y());
    return pix;
}
QRgb ScribbleArea::getColor2(int x, int y){
    QPixmap pixmap(this->size());
        this->render(&pixmap);
        QImage img(pixmap.toImage());
        QRgb pix = img.pixel(x,y);
    return pix;
}
void ScribbleArea::colorPicker(QPoint lastPoint){
        myPenColor = getColor(lastPoint);

}

void ScribbleArea::FillIn(int x, int y, QRgb currentC, QRgb newC){

    if (x < 0 || x > width()-1 || y < 0 || y > height()-1)
        return;
    QPoint p(x,y);
    QRgb pix=getColor(p);
    if (pix != currentC)
        return;
    if (pix == newC)
        return;

    // Replace the color at (x, y)
    myPenColor=newC;
    drawLineTo(p, false);

    qDebug() << "x" << x << "y" << y;
    FillIn(x+1, y, currentC, newC);
    FillIn(x-1, y, currentC, newC);
    FillIn(x, y+1, currentC, newC);
    FillIn(x, y-1, currentC, newC);

}

int ScribbleArea::validCoord(int x, int y, int n, int m)
{
    //qDebug() << "x" << x << "y" << y;
    if (x < 0 || y < 0) {
        return 0;
    }
    if (x >= n || y >= m) {
        return 0;
    }
    return 1;
}
void ScribbleArea::FillIn2(int x, int y, QRgb newC)
{
      int n=width();
      int m=height();
      int count;
      QPoint p(x,y);
      QPoint p1(x+1,y);
      QPoint p2(x-1,y);
      QPoint p3(x,y+1);
      QPoint p4(x,y-1);
      QRgb preColor=getColor(p);
      int oldWidth=myPenWidth;
      myPenWidth=1;
    // Visiting array
      int vis[m][n];

      // Initializing all as zero
      memset(vis, 0, sizeof(vis));

      // Creating queue for bfs
      std::queue<std::pair<int, int> > obj;

      // Pushing pair of {x, y}
      obj.push({ x, y });

      // Marking {x, y} as visited
      vis[x][y] = 1;

      // Until queue is empty
      while (obj.empty() != 1)
      {
        count+=1;
        qDebug() << "count" << count;
        //qDebug() << "obj length" << obj.size();

        // Extracting front pair
        std::pair<int, int> coord = obj.front();
        int x = coord.first;
        int y = coord.second;

        myPenColor=newC;
        drawLineTo2(x,y);

        // Popping front pair of queue
        obj.pop();

        // For Upside Pixel or Cell
        if (validCoord(x + 1, y, n, m)
            && vis[x + 1][y] == 0
            && getColor2(x+1,y) == preColor)
        {
          obj.push({ x + 1, y });
          vis[x + 1][y] = 1;
        }

        // For Downside Pixel or Cell
        if (validCoord(x - 1, y, n, m)
            && vis[x - 1][y] == 0
            && getColor2(x-1,y) == preColor)
        {
          obj.push({ x - 1, y });
          vis[x - 1][y] = 1;
        }

        // For Right side Pixel or Cell
        if (validCoord(x, y + 1, n, m)
            && vis[x][y + 1] == 0
            && getColor2(x,y+1) == preColor)
        {
          obj.push({ x, y + 1 });
          vis[x][y + 1] = 1;
        }

        // For Left side Pixel or Cell
        if (validCoord(x, y - 1, n, m)
            && vis[x][y - 1] == 0
            && getColor2(x,y-1) == preColor)
        {
          obj.push({ x, y - 1 });
          vis[x][y - 1] = 1;
        }
      }
      myPenWidth=oldWidth;
}

////https://github.com/SillyLossy/QtPaint/blob/master/floodfilltool.cpp
void ScribbleArea::FillIn3(int x, int y){

    //qDebug() << "fill3";
    //QImage* image = event->selectedLayer;
    QPainter painter(&image);
    painter.setPen(myPenColor.rgb());

    QPoint p(x,y);
    QRgb oldColor=getColor(p);

    // The stack of pixels to fill
    QStack<QPoint> pixels;

    // Add the initial point
    pixels.append(p);

    const int rx[] = { -1,  0, +1, 0 };
    const int ry[] = {  0, +1, 0, -1 };

    int j = 0;
    while (!pixels.empty()) {
        QPoint pt = pixels.takeLast();
        //qDebug() << "x" << pt.x() << "y" << pt.y();

        QPoint next;

        for (int i = 0; i < 4; i++) {

            next = pt;
            next.rx() = abs(next.rx()+rx[(i + j) % 4]); // abs(...) to avoid -1 when near edge
            next.ry() = abs(next.ry()+ry[(i + j) % 4]); // abs(...) to avoid -1 when near edge

            QColor pixelColor = image.pixelColor(next);
            //qDebug() << "pixelColor" << pixelColor << "oldColor" << oldColor;
            //qDebug() << "pixelColor.alpha()" << pixelColor.alpha() << "myPenColor.alpha()" << myPenColor.alpha();
            //qDebug() << (pixelColor == oldColor && pixelColor.alpha() <= myPenColor.alpha());
            if (
                    (next.x() >= 0 && next.x() < width() && next.y() >= 0 && next.y() < height())
                    &&
                    (pixelColor == oldColor && pixelColor.alpha() <= myPenColor.alpha())
                ) {
                painter.drawPoint(next);
                pixels.append(next);
            }
        }
        j++;
        // no infinite loops
        if (pixels.size() > width() * height()) {
            break;
        }
    }
    update();
}



void ScribbleArea::AddText(QPoint lastPoint, QString s){

    QPainter painter(&image);
    painter.setFont(TextFont);
    painter.setPen(myPenColor);
    painter.drawText(lastPoint, s);
    modified = true;
    update();
}
void ScribbleArea::Eraser(){
    //myOldColor=myPenColor;
    //myPenColor=Qt::white;
}



// If a mouse button is pressed check if it was the
// left button and if so store the current position
// Set that we are currently drawing
std::vector<QImage> undoVector;
void ScribbleArea::Undo(){
    if(!undoVector.empty()){
        image=undoVector.back();
        undoVector.pop_back();
    }

    update();
}
void ScribbleArea::pushUndo() {
    undoVector.push_back(image);
    if(undoVector.size()>20){
        undoVector.erase(undoVector.begin());
    }
}


void ScribbleArea::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){

        this->pushUndo();

        //qDebug() << "undo vec size" << undoVector.size();
        if (action !=5 && action !=6 && action !=7 && action != 8) // shapes magic
            lastPoint = event->pos();

        scribbling = true;
        if(action==2){
            scribbling = false;
            colorPicker(lastPoint);
            //AddText(lastPoint);
        }
        if(action==1){
            scribbling = false;
            //FillIn(lastPoint.x(),lastPoint.y(),getColor(lastPoint),myPenColor.rgb());
            //FillIn2(lastPoint.x(), lastPoint.y(), myPenColor.rgb());
            FillIn3(lastPoint.x(), lastPoint.y());
        }
        if(action==3){
            Eraser();
        }
        if(action==4){
            QString text=QInputDialog::getText(this,"Text input","Input text:");
            AddText(lastPoint,text);
        }

        // line
        if(action==5){

        }
        // ray
        if(action==6){

        }
        // rectangle
        if(action==7){

        }
        // circle
        if(action==8){
            //
        }

    } else {
        lastPoint = event->pos();
    }

}

// When the mouse moves if the left button is clicked
// we call the drawline function which draws a line
// from the last position to the current
void ScribbleArea::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && scribbling)
        drawLineTo(event->pos(), false);
}

// If the button is released we set variables to stop drawing
void ScribbleArea::mouseReleaseEvent(QMouseEvent *event)
{

    if (event->button() == Qt::LeftButton && scribbling) {
        drawLineTo(event->pos(), true);
        //scribbling = false;
        //action=0;
    }
}

// QPainter provides functions to draw on the widget
// The QPaintEvent is sent to widgets that need to
// update themselves
void ScribbleArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    // Returns the rectangle that needs to be updated
    QRect dirtyRect = event->rect();

    // Draws the rectangle where the image needs to
    // be updated
    painter.drawImage(dirtyRect, image, dirtyRect);

}

// Resize the image to slightly larger then the main window
// to cut down on the need to resize the image
void ScribbleArea::resizeEvent(QResizeEvent *event)
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}

void ScribbleArea::drawLineTo(const QPoint &endPoint, const bool onRelease)
{
    // Used to draw on the widget
    QPainter painter(&image);

    // Set the current settings for the pen
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));

    // Handles eraser
    if (action==3){
        painter.setPen(QPen(Qt::white, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                            Qt::RoundJoin));
    }
    else{
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    }

    // Draw a line from the last registered point to the current
    //if(action==1)
    //    painter.drawLine(endPoint, endPoint);
    //else
    if (action == 7){
        QPoint cornerPoint1(lastPoint.x(), endPoint.y());
        QPoint cornerPoint2(endPoint.x(), lastPoint.y());
        painter.drawLine(lastPoint, cornerPoint1);
        painter.drawLine(cornerPoint1, endPoint);
        painter.drawLine(endPoint, cornerPoint2);
        painter.drawLine(cornerPoint2, lastPoint);
    }
    else if (action == 8) {
        QRectF rectangle(lastPoint, endPoint);
        painter.drawEllipse(rectangle);
    }
    else
        painter.drawLine(lastPoint, endPoint);

    // Set that the image hasn't been saved
    modified = true;

    int rad = (myPenWidth / 2) + 2;

    // Call to update the rectangular space where we drew
    update(QRect(lastPoint, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));

    // Update the last position where we left off drawing
    // shapes magic
    if (((action == 5 || action == 7 || action == 0) && onRelease) || (action == 0)){
        lastPoint = endPoint;
    }
}
void ScribbleArea::drawLineTo2(const int x, const int y)
{
    // Used to draw on the widget
    QPainter painter(&image);

    // Set the current settings for the pen
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));

    // Draw a line from the last registered point to the current
    if(action==1)
        painter.drawPoint(x,y);

    // Set that the image hasn't been saved
    modified = true;

    int rad = (myPenWidth / 2) + 2;

    // Call to update the rectangular space where we drew
    update(QRect(x,y,x,y).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));

    // Update the last position where we left off drawing

}
// When the app is resized create a new image using
// the changes made to the image
void ScribbleArea::resizeImage(QImage *image, const QSize &newSize)
{
    // Check if we need to redraw the image
    if (image->size() == newSize)
        return;

    // Create a new image to display and fill it with white
    QImage newImage(newSize, QImage::Format_RGB32);
    newImage.fill(qRgb(255, 255, 255));

    // Draw the image
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

// Print the image

void ScribbleArea::print()
{
    // Check for print dialog availability
#if QT_FEATURE_printdialog

    // Can be used to print
    QPrinter printer(QPrinter::HighResolution);

    // Open printer dialog and print if asked
    QPrintDialog printDialog(&printer, this);
    if (printDialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = image.size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(image.rect());
        painter.drawImage(0, 0, image);
    }
#endif // QT_CONFIG(printdialog)
}

// ---------- END scribblearea.cpp ----------
