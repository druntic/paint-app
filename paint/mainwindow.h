// ---------- mainwindow.h ----------

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QList>
#include <QMainWindow>

// ScribbleArea used to paint the image
class ScribbleArea;

class MainWindow : public QMainWindow
{
    // Declares our class as a QObject which is the base class
    // for all Qt objects
    // QObjects handle events
    Q_OBJECT

public:
    MainWindow();

protected:
    // Function used to close an event
    void closeEvent(QCloseEvent *event) override;

// The events that can be triggered
private slots:
    void open();
    void save();
    void penColor();
    void penWidth();
    void about();
    void fill();
    void colorPick();
    void pencil();
    void eraser();
    void text();
    void textSettings();
    void line();
    void ray();
    void rectangle();
    void circle();
    void undo();

private:
    // Will tie user actions to functions
    void createActions();
    void createMenus();
    void clearChecks();

    // Will check if changes have occurred since last save
    bool maybeSave();

    // Opens the Save dialog and saves
    bool saveFile(const QByteArray &fileFormat);

    // What we'll draw on
    ScribbleArea *scribbleArea;

    // The menu widgets
    QMenu *saveAsMenu;
    QMenu *fileMenu;
    QMenu *optionMenu;
    QMenu *shapesMenu;
    QMenu *helpMenu;

    // All the actions that can occur
    QAction *openAct;

    // Actions tied to specific file formats
    QList<QAction *> saveAsActs;

    QAction *exitAct;
    QAction *penColorAct;
    QAction *penWidthAct;
    QAction *printAct;
    QAction *clearScreenAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QAction *fillAct;
    QAction *colorPickAct;
    QAction *pencilAct;
    QAction *eraserAct;
    QAction *textAct;
    QAction *textSettingsAct;
    QAction *lineAct;
    QAction *rayAct;
    QAction *rectangleAct;
    QAction *circleAct;
    QAction *undoAct;
;};

#endif

// ---------- END mainwindow.h ----------
