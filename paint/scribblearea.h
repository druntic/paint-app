// ---------- scribblearea.h ----------

#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>

class ScribbleArea : public QWidget
{
    // Declares our class as a QObject which is the base class
    // for all Qt objects
    // QObjects handle events
    Q_OBJECT

public:
    ScribbleArea(QWidget *parent = 0);

    // Handles all events
    bool openImage(const QString &fileName);
    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);
    void setFillvariable();
    void setColorPickvariable();
    void setPencilvariable();
    void setEraservariable();
    void setTextvariable();

    void setLinevariable();
    void setRayvariable();
    void setRectanglevariable();
    void setCirclevariable();
    void setTextFont(QFont NewText);

    void colorPicker(const QPoint lastPoint);
    QRgb getColor(const QPoint lastPoint);
    QRgb getColor2(int x, int y);
    void FillIn(int x, int y, QRgb currentC, QRgb newC);
    void FillIn2(int x, int y, QRgb newC);
    void FillIn3(int x, int y);
    void Eraser();
    void Undo();
    void pushUndo();
    int validCoord(int x, int y, int n, int m);
    void AddText(QPoint lastPoint, QString s);


    // Has the image been modified since last save
    bool isModified() const { return modified; }
    QColor penColor() const { return myPenColor; }
    int penWidth() const { return myPenWidth; }
public slots:

    // Events to handle
    void clearImage();
    void print();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    // Updates the scribble area where we are painting
    void paintEvent(QPaintEvent *event) override;

    // Makes sure the area we are drawing on remains
    // as large as the widget
    void resizeEvent(QResizeEvent *event) override;

private:
    void drawLineTo(const QPoint &endPoint, const bool onRelease);
    void drawLineTo2(const int x, const int y);
    void drawPixelTo(const QPoint &endPoint);
    void resizeImage(QImage *image, const QSize &newSize);

    // Will be marked true or false depending on if
    // we have saved after a change
    bool modified;

    // Marked true or false depending on if the user
    // is drawing
    bool scribbling;
    // Holds the current pen width & color
    int myPenWidth;

    int action;
    QColor myPenColor;
    QColor myOldColor;
    QFont CurrentFont;
    QFont TextFont;
    // Stores the image being drawn
    QImage image;

    // Stores the location at the current mouse event
    QPoint lastPoint;
};

#endif

// ---------- END scribblearea.h ----------
