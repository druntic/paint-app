// ---------- mainwindow.cpp ----------

#include <QtWidgets>
#include <QFontDialog>
#include "mainwindow.h"
#include "scribblearea.h"

// MainWindow constructor
MainWindow::MainWindow()
{
    // Create the ScribbleArea widget and make it
    // the central widget
    scribbleArea = new ScribbleArea;
    setCentralWidget(scribbleArea);

    // Create actions and menus
    createActions();
    createMenus();

    // Set the title
    setWindowTitle(tr("Scribble"));

    // Size the app
    resize(500, 500);
}

// User tried to close the app
void MainWindow::closeEvent(QCloseEvent *event)
{
    // If they try to close maybeSave() returns true
    // if no changes have been made and the app closes
    if (maybeSave()) {
        event->accept();
    } else {

        // If there have been changes ignore the event
        event->ignore();
    }
}

// Check if the current image has been changed and then
// open a dialog to open a file
void MainWindow::open()
{
    // Check if changes have been made since last save
    // maybeSave() returns true if no changes have been made
    if (maybeSave()) {

        // Get the file to open from a dialog
        // tr sets the window title to Open File
        // QDir opens the current dirctory
        QString fileName = QFileDialog::getOpenFileName(this,
                                   tr("Open File"), QDir::currentPath());

        // If we have a file name load the image and place
        // it in the scribbleArea
        if (!fileName.isEmpty())
            scribbleArea->openImage(fileName);
    }
}

// Called when the user clicks Save As in the menu
void MainWindow::save()
{
    // A QAction represents the action of the user clicking
    QAction *action = qobject_cast<QAction *>(sender());

    // Stores the array of bytes of the users data
    QByteArray fileFormat = action->data().toByteArray();

    // Pass it to be saved
    saveFile(fileFormat);
}

// Opens a dialog to change the pen color
void MainWindow::penColor()
{

    // Store the chosen color from the dialog
    QColor newColor = QColorDialog::getColor(scribbleArea->penColor());
    //QFont newText=QFontDialog::getFont(0, this);
    //scribbleArea->setTextFont(newText);
    // If a valid color set it
    if (newColor.isValid())
        scribbleArea->setPenColor(newColor);
}
void MainWindow::textSettings(){
    bool ok;
    QFont newText=QFontDialog::getFont(&ok, this);
    if(ok)
        scribbleArea->setTextFont(newText);
}
// Opens a dialog that allows the user to change the pen width
void MainWindow::penWidth()
{
    // Stores button value
    bool ok;

    // tr("Scribble") is the title
    // the next tr is the text to display
    // Get the current pen width
    // Define the min, max, step and ok button
    int newWidth = QInputDialog::getInt(this, tr("Scribble"),
                                        tr("Select pen width:"),
                                        scribbleArea->penWidth(),
                                        1, 50, 1, &ok);
    // Change the pen width
    if (ok)
        scribbleArea->setPenWidth(newWidth);
}

// Open an about dialog
void MainWindow::about()
{
    // Window title and text to display
    QMessageBox::about(this, tr("About Shapes"),
            tr("<p>The Right click sets a new point without drawing.</p>"));
}

void clearChecks(QMenu *optionMenu){
    QList list=optionMenu->actions();
    foreach (QAction* action, list)
    {
        if(action->isCheckable()){
        action->setChecked(false);
        }
    }
}
void MainWindow::fill()
{
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    fillAct->setChecked(true);
    scribbleArea->setFillvariable();
}
void MainWindow::colorPick()
{
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    colorPickAct->setChecked(true);
    scribbleArea->setColorPickvariable();
}
void MainWindow::pencil()
{
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    pencilAct->setChecked(true);
    scribbleArea->setPencilvariable();
}
void MainWindow::eraser(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    eraserAct->setChecked(true);
    scribbleArea->setEraservariable();
}
void MainWindow::text(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    textAct->setChecked(true);
    scribbleArea->setTextvariable();
}

void MainWindow::line(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    lineAct->setChecked(true);
    scribbleArea->setLinevariable();
}
void MainWindow::ray(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    rayAct->setChecked(true);
    scribbleArea->setRayvariable();
}
void MainWindow::rectangle(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    rectangleAct->setChecked(true);
    scribbleArea->setRectanglevariable();
}
void MainWindow::circle(){
    ::clearChecks(optionMenu);
    ::clearChecks(shapesMenu);
    circleAct->setChecked(true);
    scribbleArea->setCirclevariable();
}
void MainWindow::undo(){
    scribbleArea->Undo();
}


// Define menu actions that call functions
void MainWindow::createActions()
{
    // Create the action tied to the menu
    openAct = new QAction(tr("&Open..."), this);

    // Define the associated shortcut key
    openAct->setShortcuts(QKeySequence::Open);

    // Tie the action to MainWindow::open()
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    // Get a list of the supported file formats
    // QImageWriter is used to write images to files
    foreach (QByteArray format, QImageWriter::supportedImageFormats()) {
        QString text = tr("%1...").arg(QString(format).toUpper());

        // Create an action for each file format
        QAction *action = new QAction(text, this);

        // Set an action for each file format
        action->setData(format);

        // When clicked call MainWindow::save()
        connect(action, SIGNAL(triggered()), this, SLOT(save()));

        // Attach each file format option menu item to Save As
        saveAsActs.append(action);
    }


    // Create exit action and tie to MainWindow::close()
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    // Create pen color action and tie to MainWindow::penColor()
    penColorAct = new QAction(tr("&Pen Color..."), this);
    connect(penColorAct, SIGNAL(triggered()), this, SLOT(penColor()));

    // Create Text settings action and tie to MainWindow::TextSettings()
    textSettingsAct = new QAction(tr("&Text options..."), this);
    connect(textSettingsAct, SIGNAL(triggered()), this, SLOT(textSettings()));

    // Create pen width action and tie to MainWindow::penWidth()
    penWidthAct = new QAction(tr("Pen &Width..."), this);
    connect(penWidthAct, SIGNAL(triggered()), this, SLOT(penWidth()));

    // Create colorPick action and ..
    colorPickAct = new QAction(tr("Color picker"), this);
    colorPickAct->setCheckable(true);
    connect(colorPickAct, SIGNAL(triggered()), this, SLOT(colorPick()));

    // Create pencil action action and ..
    pencilAct = new QAction(tr("Pencil"), this);
    pencilAct->setCheckable(true);
    pencilAct->setChecked(true);
    connect(pencilAct, SIGNAL(triggered()), this, SLOT(pencil()));

    // Create fill action and ..
    fillAct = new QAction(tr("Fill in"), this);
    fillAct->setCheckable(true);
    connect(fillAct, SIGNAL(triggered()), this, SLOT(fill()));

    // Create eraser action and ..
    eraserAct = new QAction(tr("Eraser"), this);
    eraserAct->setCheckable(true);
    connect(eraserAct, SIGNAL(triggered()), this, SLOT(eraser()));

    // Create text action and ..
    textAct = new QAction(tr("Text"), this);
    textAct->setCheckable(true);
    connect(textAct, SIGNAL(triggered()), this, SLOT(text()));
    // Create undo action and ..
    undoAct = new QAction(tr("Undo"), this);
    undoAct->setShortcut(tr("Ctrl+Z"));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    // Create shape action and ..
    lineAct = new QAction(tr("Line"), this);
    lineAct->setCheckable(true);
    connect(lineAct, SIGNAL(triggered()), this, SLOT(line()));

    // Create shape action and ..
    rayAct = new QAction(tr("Ray"), this);
    rayAct->setCheckable(true);
    connect(rayAct, SIGNAL(triggered()), this, SLOT(ray()));

    // Create shape action and ..
    rectangleAct = new QAction(tr("Rectangle"), this);
    rectangleAct->setCheckable(true);
    connect(rectangleAct, SIGNAL(triggered()), this, SLOT(rectangle()));

    // Create shape action and ..
    circleAct = new QAction(tr("Ellipse"), this);
    circleAct->setCheckable(true);
    connect(circleAct, SIGNAL(triggered()), this, SLOT(circle()));


    // Create clear screen action and tie to MainWindow::clearImage()
    clearScreenAct = new QAction(tr("&Clear Screen"), this);
    clearScreenAct->setShortcut(tr("Ctrl+L"));
    connect(clearScreenAct, SIGNAL(triggered()),
            scribbleArea, SLOT(clearImage()));

    // Create about action and tie to MainWindow::about()
    aboutAct = new QAction(tr("&About Shapes"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    // Create about Qt action and tie to MainWindow::aboutQt()
    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

// Create the menubar
void MainWindow::createMenus()
{
    // Create Save As option and the list of file types
    saveAsMenu = new QMenu(tr("&Save As"), this);
    foreach (QAction *action, saveAsActs)
        saveAsMenu->addAction(action);

    // Attach all actions to File
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(openAct);
    fileMenu->addMenu(saveAsMenu);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    // Attach all actions to Options
    optionMenu = new QMenu(tr("&Options"), this);
    optionMenu->addAction(penColorAct);
    optionMenu->addAction(penWidthAct);
    optionMenu->addAction(textSettingsAct);
    optionMenu->addSeparator();
    optionMenu->addAction(colorPickAct);
    optionMenu->addAction(pencilAct);
    optionMenu->addAction(fillAct);
    optionMenu->addAction(eraserAct);
    optionMenu->addAction(textAct);
    optionMenu->addSeparator();
    optionMenu->addAction(clearScreenAct);
    optionMenu->addAction(undoAct);

    // Attach all actions to Shapes
    shapesMenu = new QMenu(tr("&Shapes"), this);
    shapesMenu->addAction(lineAct);
    shapesMenu->addAction(rayAct);
    shapesMenu->addAction(rectangleAct);
    shapesMenu->addAction(circleAct);

    // Attach all actions to Help
    helpMenu = new QMenu(tr("&Help"), this);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    // Add menu items to the menubar
    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(optionMenu);
    menuBar()->addMenu(shapesMenu);
    menuBar()->addMenu(helpMenu);
}

bool MainWindow::maybeSave()
{
    // Check for changes since last save
    if (scribbleArea->isModified()) {
       QMessageBox::StandardButton ret;

       // Scribble is the title
       // Add text and the buttons
       ret = QMessageBox::warning(this, tr("Scribble"),
                          tr("The image has been modified.\n"
                             "Do you want to save your changes?"),
                          QMessageBox::Save | QMessageBox::Discard
                          | QMessageBox::Cancel);

       // If save button clicked call for file to be saved
        if (ret == QMessageBox::Save) {
            return saveFile("png");

        // If cancel do nothing
        } else if (ret == QMessageBox::Cancel) {
            return false;
        }
    }
    return true;
}

bool MainWindow::saveFile(const QByteArray &fileFormat)
{
    // Define path, name and default file type
    QString initialPath = QDir::currentPath() + "/untitled." + fileFormat;

    // Get selected file from dialog
    // Add the proper file formats and extensions
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                               initialPath,
                               tr("%1 Files (*.%2);;All Files (*)")
                               .arg(QString::fromLatin1(fileFormat.toUpper()))
                               .arg(QString::fromLatin1(fileFormat)));

    // If no file do nothing
    if (fileName.isEmpty()) {
        return false;
    } else {

        // Call for the file to be saved
        return scribbleArea->saveImage(fileName, fileFormat.constData());
    }
}

// ---------- END mainwindow.cpp ----------
